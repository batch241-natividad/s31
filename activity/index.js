/*Part 1

1. What directive is used by Node.js in loading the modules it needs?

- The "require" directive.

2. What Node.js module contains a method for server creation?

-The HTTP module

3. What is the method of the http object responsible for creating a server using Node.js?

-http.createServer(function(){})

4. What method of the response object allows us to set status codes and content types?

-response.WriteHead()

5. Where will console.log() output its contents when run in Node.js?

-At the terminal

6. What property of the request object contains the address's endpoint?
-The 'url'

*/


const http = require('http');

const port = 3000;

const server = http.createServer(function(request, response){

	if(request.url == '/login'){
		response.writeHead(200, {'Content-type': 'text/plain'})
		response.end('Welcome to login page')
	} 

	

	 else {
		response.writeHead(404, {'Content-type': 'text/plain'})
		response.end('Im sorry the page you are looking for is not available')
	}
})

server.listen(port);

console.log(`Server is now running at localhost: ${port}`)